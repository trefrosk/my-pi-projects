# This is just a test file.  
# We will try to use the outputs first, namely an LED.

import RPi.GPIO as GPIO
import time

LEDpin = 13
timer = .5

GPIO.setup(LEDpin, GPIO.OUT)
while True:
    GPIO.output(LEDpin, True)
    time.sleep(2)
    GPIO.output(LEDpin, False)
    time.sleep(25)
    
