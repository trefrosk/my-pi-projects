# Testing button push on Raspberry Pi

import RPi.GPIO as GPIO
import time

GPIO.setup(12, GPIO.IN)
GPIO.setup(13, GPIO.OUT)

while True:
    input_value = GPIO.input(12)
    GPIO.output(13, False)
    
    if input_value == False:
        print ("The button has been pressed.")
        time.sleep(2)
        GPIO.output(13, True)
        time.sleep(2)
        GPIO.output(13, False)
        time.sleep(2)
        GPIO.output(13, True)
        time.sleep(2)
        
        
#        while input_value == False:
#            input_value = GPIO.input(12)
            
            